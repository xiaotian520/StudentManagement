package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.student.model.Score;
import com.student.model.sscore;
import com.student.utils.DBUtils;

public class ScoreDao {
	
	public List<sscore> SearchCourse(String CourseNo){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<sscore> list = new ArrayList<sscore>();
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			
			//sql语句
			String sql = "select t_user.sNo,t_user.Sname,score.grade from t_user,score where score.cNo= ? and t_user.Sno = score.sNo and userType = 2";
			//获取Statement对象
			ps = conn.prepareStatement(sql);
			//设置参数
			ps.setString(1, CourseNo);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			while(rs.next()){
				sscore user = new sscore();
				user.setSno(rs.getString("Sno"));
				user.setSname(rs.getString("Sname"));
				user.setGrade(rs.getString("grade"));
				list.add(user);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return list;
	}
	
	/**
	 * 
	 * @param courseNo
	 * @param StudentNo
	 * @param score
	 * @return
	 */
	public boolean updateScore(String courseNo,String StudentNo,String score) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "update score set grade=" + "'"+ score + "'" +"where sNo= "+ "'" + StudentNo +"'"+ " and cNo ="+ "'" +courseNo +"'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}

	public List<Score> SearchSno(String sno) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Score> list = new ArrayList<Score>();
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			
			//sql语句
			String sql = "select * from score where sNo = ?";
			//获取Statement对象
			ps = conn.prepareStatement(sql);
			//设置参数
			ps.setString(1, sno);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			while(rs.next()){
				Score score = new Score();
				score.setsNo(rs.getString("sNo"));
				score.setcNo(rs.getString("cNo"));
				score.setGrade(rs.getString("grade"));
				list.add(score);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return list;
	}
	

}
