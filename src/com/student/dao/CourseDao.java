package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import com.student.model.Course;
import com.student.utils.DBUtils;

public class CourseDao {

	public List<Course> SelectCourse(String cNo, String cName) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Course> list=new ArrayList<Course>();
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "select * from course where 1=1";
			if(cNo!=null && !"".equals(cNo)){
				sql+= " and cNo = '" + cNo+"'";
			}
			if(cName!=null && !"".equals(cName)){
				sql+= " and cName = '" + cName+"'";
			}
			ps = conn.prepareStatement(sql);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			while(rs.next()){
				Course course = new Course();
					course.setcNo(rs.getString("cNo"));
					course.setcName(rs.getString("cName"));
					course.setTeaNo(rs.getString("teaNo"));
	                list.add(course);
	    	}
		} catch (SQLException e) {
				e.printStackTrace();
	    		JOptionPane.showMessageDialog(null, "没有查询到该课程！");
	    	
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return list;
	}
	
	/**
	 * 通过课程号和课程名查询课程是否存在
	 * @param cNo
	 * @param cName
	 * @return
	 */
	public Course isExistCourse(String cNo, String cName) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Course course = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "select * from course where cNo = ? or cName = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, cNo);
			ps.setString(2, cName);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			while(rs.next()){
				course = new Course();
				course.setcNo(rs.getString("cNo"));
				course.setcName(rs.getString("cName"));
				course.setTeaNo(rs.getString("teaNo"));
	    	}
		} catch (SQLException e) {
	    		JOptionPane.showMessageDialog(null, "添加失败，该学生已存在！");
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return course;
	}

	/**
	 * 添加课程
	 * @param cNo
	 * @param cName
	 * @return
	 */
	public boolean AddCourse(String cNo, String cName) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "INSERT INTO course (cNo,cName) VALUES ('"+cNo+"','"+cName+"')";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	
	/**
	 * 更新课程
	 * @param ucNo
	 * @param ucName
	 * @param uteaNo
	 * @return
	 */
	public boolean UpdateCourse(String ucNo, String ucName, String uteaNo) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "update course set cName=" + "'"+ ucName + "'" + ",teaNo=" + "'"+ uteaNo + "'" + "where cNo=" + "'" + ucNo + "'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}

	/**
	 * 删除课程
	 * @param dcNo
	 * @return
	 */
	public boolean DeleteCourse(String dcNo) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "delete from course where cNo = " + "'" + dcNo + "'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	
	
	/**
	 * 为学生添加课程
	 * @param course
	 * @param major
	 * @return
	 */
	public boolean addStudentCourse(String course,String major) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "insert into score(sNo,cNo) select sno,cNo from t_user,course where  t_user.major=" + "'"+ major + "'" +"and course.cName= "+ "'" + course +"'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}


}
