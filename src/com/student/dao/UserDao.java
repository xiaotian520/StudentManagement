package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.student.model.User;
import com.student.utils.DBUtils;

public class UserDao {
	
	/**
	 * 通过用户名，密码用户类型登录
	 * @param username
	 * @param password
	 * @param userType
	 * @return
	 */
	public User login(String username,String password,String userType)
	{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			
			//sql语句
			String sql = "select * from t_user where Sno = ? and Spassword = ? and userType = ?";
			//获取Statement对象
			ps = conn.prepareStatement(sql);
			//设置参数
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, userType);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			if(rs.next()){
				user = new User();
				user.setSno(rs.getString("Sno"));
				user.setSpassword(rs.getString("Spassword"));
				user.setSname(rs.getString("Sname"));
				user.setSsex(rs.getString("Ssex"));
				user.setSage(rs.getString("Sage"));
				user.setMajor(rs.getString("Major"));
				user.setUserType(rs.getString("userType"));
	    	}
			System.out.println(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return user;
	}
	/**
	 * 添加一个学生
	 * @param sno
	 * @param pass
	 * @param sname
	 * @param sex
	 * @param sage
	 * @param major
	 * @return
	 */
	public boolean AddStudent(String sno,String pass,String sname,String sex,String sage,String major) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "INSERT INTO t_user (Sno,Spassword,Sname,Ssex,Sage,Major) VALUES ('"+sno+"','"+pass+"','"+sname+"','"+sex+"','"+sage+"','"+major+"')";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	
	/**
	 * 通过编号查找老师
	 * @param sno
	 * @return
	 */
	public List<User> SelectTeacher(String sno) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<User> list=new ArrayList<User>();
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "select * from t_user where 1=1 and userType = 1";
			if(sno!=null && !"".equals(sno)){
				sql = "select * from t_user where 1=1 and Sno = '" + sno+"'" + "and userType = 1";
			}
			ps = conn.prepareStatement(sql);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			while(rs.next()){
				User user=new User();
					user.setSno(rs.getString("Sno"));
					user.setSpassword(rs.getString("Spassword"));
					user.setSname(rs.getString("Sname"));
					user.setSsex(rs.getString("Ssex"));
					user.setSage(rs.getString("Sage"));
					user.setMajor(rs.getString("Major"));
	                list.add(user);
	    	}
		} catch (SQLException e) {
				e.printStackTrace();
	    		JOptionPane.showMessageDialog(null, "没有该老师！");
	    	
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return list;
	}
	
	/**
	 * 通过学号查询学生
	 * @param sno
	 * @return
	 */
	public List<User> SelectStudent(String sno) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<User> list=new ArrayList<User>();
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "select * from t_user where 1=1 and userType = 2";
			if(sno!=null && !"".equals(sno)){
				sql = "select * from t_user where 1=1 and Sno = '" + sno+"'";
			}
			ps = conn.prepareStatement(sql);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			while(rs.next()){
				User user=new User();
					user.setSno(rs.getString("Sno"));
					user.setSpassword(rs.getString("Spassword"));
					user.setSname(rs.getString("Sname"));
					user.setSsex(rs.getString("Ssex"));
					user.setSage(rs.getString("Sage"));
					user.setMajor(rs.getString("Major"));
	                list.add(user);
	    	}
		} catch (SQLException e) {
				e.printStackTrace();
	    		JOptionPane.showMessageDialog(null, "没有该学生！");
	    	
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return list;
	}
	/**
	 * 通过学号删除学生
	 * @param sno
	 * @return
	 */
	public boolean DeleteStudent(String sno) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "delete from t_user where Sno = " + "'" + sno + "'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	public boolean UpdateStudent(String sno, String spass, String sname, String ssex, String sage, String major) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "update t_user set Sname=" + "'"+ sname + "'" + ",Spassword=" + "'"+ spass + "'" + ",Ssex=" + "'"+ ssex + "'" + ",Sage=" + "'"+ sage + "'" + ",Major=" + "'"+ major + "'" + "where Sno="+ "'" +sno +"'";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	/**
	 * 学生是否存在
	 * @param sno
	 * @return
	 */
	public User isExistUser(String sno) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "select * from t_user where Sno = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, sno);
			//执行查询，把数据库响应的查询结果存放在ResultSet中。
			rs = ps.executeQuery();
			//遍历
			while(rs.next()){
				user = new User();
				user.setSno(rs.getString("Sno"));
				user.setSpassword(rs.getString("Spassword"));
				user.setSname(rs.getString("Sname"));
				user.setSsex(rs.getString("Ssex"));
				user.setSage(rs.getString("Sage"));
				user.setMajor(rs.getString("Major"));
	    	}
		} catch (SQLException e) {
	    		JOptionPane.showMessageDialog(null, "添加失败，该学生已存在！");
		}finally{
			//关闭资源
			DBUtils.close(rs, ps, conn);
		}
		return user;
	}
	/**
	 * 更新密码
	 * @param sno
	 * @param npass
	 */
	public boolean Updatepassword(String sno, String npass) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "update t_user set Spassword=" + "'"+ npass + "'" + " where Sno=" + "'"+ sno + "'";
			ps = conn.prepareStatement(sql);
			System.out.println(sql);
            int r = ps.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	/**
	 * 添加教师
	 * @param sno
	 * @param spass
	 * @param sname
	 * @param ssex
	 * @param sage
	 * @param major
	 * @return
	 */
	public boolean AddTeacher(String sno, String spass, String sname, String ssex, String sage, String major) {
		Connection conn = null;
		PreparedStatement ps = null;
		//JDBC操作
		//获取连接
		try {
			conn = DBUtils.getConnection();
			//获取Statement对象
			String sql = "INSERT INTO t_user (Sno,Spassword,Sname,Ssex,Sage,Major,userType) VALUES ('"+sno+"','"+spass+"','"+sname+"','"+ssex+"','"+sage+"','"+major+"',"+'1'+")";
			ps = conn.prepareStatement(sql);
            int r = ps.executeUpdate();
            System.out.println(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			//关闭资源
			DBUtils.close(null, ps, conn);
		}
		return false;
	}
	
}
