package com.student.view;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import javax.swing.*;

import com.student.dao.UserDao;
import com.student.utils.UserUtil;


public class MainFrame extends JFrame implements ActionListener,Runnable{
	
	private SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd E");
	
	private JLabel datetime;
	private JLabel dateweek;
	private JLabel welcome;
	
	UserDao uDao = new UserDao();
	
	JMenuBar  menubar = new JMenuBar();
	
	JMenu personalMenu = new JMenu("个人中心");
	
	JMenuItem  infoMenuItem = new JMenuItem("我的信息");
	JMenuItem  passMenuItem = new JMenuItem("修改密码");
	JMenuItem  exitMenuItem = new JMenuItem("退出系统");
	JMenuItem AboutUsMenuItem = new JMenuItem("关于我们");
	
	JMenu   userMenu = new JMenu("用户管理");
	JMenuItem  teacherMenuItem = new JMenuItem("教师用户管理");
	JMenuItem addStudentMenuItem = new JMenuItem("添加学生");
	JMenuItem updateStudentMenuItem = new JMenuItem("学生修改");

	JMenu   gradeMenu = new JMenu("成绩管理");
	JMenuItem inputMenuItem = new JMenuItem("成绩录入");
	JMenuItem queryMenuItem = new JMenuItem("成绩查询");

	JMenu   courseMenu = new JMenu("课程管理");
	JMenuItem courseCatalogMenuItem = new JMenuItem("开课目录管理");
	JMenuItem studentListMenuItem = new JMenuItem("课程名单管理");
	
	
	JMenu   studentGradeMenu = new JMenu("成绩查询");
	JMenuItem querygradeMenuItem = new JMenuItem("学生成绩查询");
	
	MyInfoDialog myInfoDialog;
	TeacherManage teacherManage;
	UpdatePassword updatePassword;
	InputScore inputScoreDialog;
	QueryScore queryScoreDialog;
	CourseManage addCourseDialog;
	Coursecatalogue coursecatalogueDialog;
	InputStudent inputstudent;
	StudentManage studentmanage;
	StudentQueryScore studentqueryscore;
	AboutUs aboutus;
	public MainFrame(){
		this.setTitle("学生成绩管理系统");
		this.setResizable(false);
		//构造主菜单
		this.setJMenuBar(menubar);
		menubar.add(personalMenu);
		menubar.add(userMenu);
		menubar.add(courseMenu);
		menubar.add(gradeMenu);
		menubar.add(studentGradeMenu);
		
		//构造个人信息菜单
		personalMenu.add(infoMenuItem);
		personalMenu.add(passMenuItem);
		personalMenu.add(exitMenuItem);
		personalMenu.add(AboutUsMenuItem);
		
		//构造用户管理菜单
		userMenu.add(teacherMenuItem);
		userMenu.add(addStudentMenuItem);
		userMenu.add(updateStudentMenuItem);
		//构造成绩管理菜单
		gradeMenu.add(inputMenuItem);
		gradeMenu.add(queryMenuItem);
		//构造课程管理菜单
		courseMenu.add(courseCatalogMenuItem);
		courseMenu.add(studentListMenuItem);
		//构造学生管理菜单
		//学生成绩查询
		studentGradeMenu.add(querygradeMenuItem);

		//为主菜单注册监听器
		infoMenuItem.addActionListener(this);
		passMenuItem.addActionListener(this);
		exitMenuItem.addActionListener(this);
		AboutUsMenuItem.addActionListener(this);
		
		teacherMenuItem.addActionListener(this);
		addStudentMenuItem.addActionListener(this);
		updateStudentMenuItem.addActionListener(this);
		
		inputMenuItem.addActionListener(this);
		queryMenuItem.addActionListener(this);
		
		courseCatalogMenuItem.addActionListener(this);
		studentListMenuItem.addActionListener(this);
		
		querygradeMenuItem.addActionListener(this);
		
		//显示系统主界面
		this.setSize(600, 400);
		if(UserUtil.Sname != null){
			welcome = new JLabel("欢迎你");
			welcome.setBounds(0, -30, 600, 100);
			welcome.setFont(new Font("华文楷体", Font.ITALIC, 30));
			welcome.setText("欢迎你："+UserUtil.Sname);// 显示用户名
			this.getContentPane().add(welcome);
		}
		dateweek = new JLabel("今天是"+date.format(new Date()));
		dateweek.setBounds(300, -35, 350, 100);
		dateweek.setFont(new Font("华文隶书", Font.LAYOUT_LEFT_TO_RIGHT, 26));
		this.getContentPane().add(dateweek);
		datetime = new JLabel("");
		datetime.setBounds(460, 5, 110, 80);
		datetime.setFont(new Font("华文隶书", Font.LAYOUT_LEFT_TO_RIGHT, 32));
		this.getContentPane().add(datetime);
		JLabel lbBg = new JLabel(new ImageIcon("images/bg.jpg"));
		lbBg.setBounds(0, 0, 600, 400);
		this.getContentPane().add(lbBg);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		if("1".equals(UserUtil.userType)){
			userMenu.setEnabled(false);
			courseMenu.setEnabled(false);
		}else if("2".equals(UserUtil.userType)){
			userMenu.setEnabled(false);
			gradeMenu.setEnabled(false);
			courseMenu.setEnabled(false);
		}
		
	}
	public static void main(String[] args) {
		 MainFrame mf = new MainFrame();
		
		 Thread thread1=new Thread(mf);
		 thread1.start();
	}
	
	public void actionPerformed(ActionEvent e) {
		
		//主菜单控制
		if(e.getSource()==infoMenuItem){//我的信息
			if(myInfoDialog==null){
				myInfoDialog = new MyInfoDialog(this);
			}
			myInfoDialog.setVisible(true);
		}else if(e.getSource()==passMenuItem){//修改密码
			if(updatePassword==null){
				updatePassword = new UpdatePassword(this);
			}
			updatePassword.setVisible(true);
		}else if(e.getSource()==exitMenuItem){//退出系统
			if(JOptionPane.showConfirmDialog(this, "确认要退出系统？","退出",JOptionPane.OK_CANCEL_OPTION)==JOptionPane.OK_OPTION){
				System.exit(0);
			}			
		}else if(e.getSource()==AboutUsMenuItem){//关于我们
			if(aboutus==null){
				aboutus = new AboutUs(this);
			}
			aboutus.setVisible(true);
		}else if(e.getSource()==teacherMenuItem){//教师用户管理
			if(teacherManage==null){
				teacherManage = new TeacherManage(this);
			}
			teacherManage.setVisible(true);
		}else if(e.getSource()==addStudentMenuItem){//添加学生
			//创建添加学生面板
			if(inputstudent==null){
				inputstudent = new InputStudent(this);
			}
			inputstudent.setVisible(true);
		}else if(e.getSource()==updateStudentMenuItem){//更新学生信息
			//更新学生信息
			if(studentmanage==null){
				studentmanage = new StudentManage(this);
			}
			studentmanage.setVisible(true);
		}else if(e.getSource()==inputMenuItem){//录入成绩
			//创建成绩录入面板
			if(inputScoreDialog==null){
				inputScoreDialog = new InputScore(this);
			}
			inputScoreDialog.setVisible(true);
		}else if(e.getSource()==queryMenuItem){//查询成绩
			//创建成绩查询面板
			if(queryScoreDialog==null){
				queryScoreDialog = new QueryScore(this);
			}
			queryScoreDialog.setVisible(true);
		}else if(e.getSource()==courseCatalogMenuItem){//开课目录
			//开课目录
			if(addCourseDialog==null){
				addCourseDialog = new CourseManage(this);
			}
			addCourseDialog.setVisible(true);
		}else if(e.getSource()==studentListMenuItem){//课程名单
			//课程名单
			if(coursecatalogueDialog==null){
				coursecatalogueDialog = new Coursecatalogue(this);
			}
			coursecatalogueDialog.setVisible(true);
		}else if(e.getSource()==querygradeMenuItem){//学生查询成绩
			//学生查询成绩
			if(studentqueryscore==null){
			studentqueryscore = new StudentQueryScore(this);
			}
			studentqueryscore.setVisible(true);
		}
			
	}
	
	
	
	
	public void run(){
		while(true){
			 SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
			 datetime.setText(time.format(Calendar.getInstance().getTime()));
		 try{
			 Thread.sleep(1000); 
		 }catch(Exception e){
			 datetime.setText("Error!!!");
		 }
		}
	}
	
	class DateAndTimeUpdate extends TimerTask {
		public void run() {
			// 刷新时间
			repaint();
		}
	}
		public void paint(Graphics g) {// swing Graphics类绘图
			super.paint(g);
			
			g.setColor(new Color(255, 245, 225));
			g.setFont(new Font("华文行楷", Font.ITALIC, 50));//系统名
			g.drawString("托普学生成绩管理系统", 50, 250);
			
			g.setColor(new Color(255, 245, 225));
			g.setFont(new Font("华文楷体", Font.ITALIC, 30));// 署名
			g.drawString("Designed by xiaotian", 300, 300);
 
 
		}
}