package com.student.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.student.dao.UserDao;
import com.student.model.User;
import com.student.utils.UserUtil;

public class LoginFrame extends JFrame implements ActionListener{
	
	JTextField  jtfUser = new JTextField(10);
	JPasswordField  jtfPass = new JPasswordField(10);
	JTextField jtfCode = new JTextField(10);
	private ValidateCode vcode;
	JComboBox  jcbUserType = new JComboBox();
	JButton  jbtOk = new JButton("登录");
	JButton  jbtCancel = new JButton("退出");
	UserDao userDao = new UserDao();
	
	public LoginFrame(String title){
		super();
		this.setTitle("登录");
		JPanel  p1 = new JPanel();
		JPanel  p2 = new JPanel();
		JPanel  p3 = new JPanel();
		JPanel  p4 = new JPanel();
		JPanel  p5 = new JPanel();
		JPanel  p6 = new JPanel();
		JPanel  p7 = new JPanel();
		JLabel lbBg = new JLabel(new ImageIcon("images/login_pic.png"));
		lbBg.setBounds(0, 0, lbBg.WIDTH, lbBg.HEIGHT);
		p1.add(lbBg);
		JLabel welcome = new JLabel("欢迎使用本系统");
		welcome.setBounds(100, 80, 600, 100);
		welcome.setFont(new Font("华文楷体", Font.ITALIC, 30));
		this.getContentPane().add(welcome);
		p2.setLayout(new FlowLayout(FlowLayout.CENTER,100,10));
		p2.add(p4);
		p2.add(p5);
		p2.add(p6);
		p2.add(p7);
		p4.add(new JLabel("用户名："));
		p4.add(jtfUser);
		p5.add(new JLabel("密    码："));
		p5.add(jtfPass);
		p6.add(new JLabel("用户类别："));
		jcbUserType.addItem("学生");
		jcbUserType.addItem("教师");
		jcbUserType.addItem("教务管理员");
		jcbUserType.setSelectedIndex(0);
		p6.add(jcbUserType);
		p7.add(new JLabel("验证码"));
		p7.add(jtfCode);
		vcode = new ValidateCode();
		this.getContentPane().add(vcode);
		vcode.setBounds(300, 410, 100, 100);
		System.out.println(vcode);
		p3.add(jbtOk);
		p3.add(jbtCancel);
		KeyListener key_Listener = new KeyListener()
		{
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e){}
			public void keyPressed(KeyEvent e){
				if(e.getKeyChar() == KeyEvent.VK_ENTER )
				{
					login();
				}
			}
		};
		jtfUser.addKeyListener(key_Listener);
		jtfPass.addKeyListener(key_Listener);
		jcbUserType.addKeyListener(key_Listener);
		jtfCode.addKeyListener(key_Listener);
		
		this.getContentPane().add(p1,BorderLayout.NORTH);
		this.getContentPane().add(p2,BorderLayout.CENTER);
		this.getContentPane().add(p3,BorderLayout.SOUTH);
		jbtOk.addActionListener(this);
		jbtCancel.addActionListener(this);
		this.setSize(430,500);
		this.setUndecorated(true); // 去掉窗口的装饰 
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbtOk){
			login();
		}else if(e.getSource()==jbtCancel){
			System.exit(0);
		}
	}
	private void login() {
		if(!isValidCodeRight()){
			JOptionPane.showMessageDialog(null, "验证码错误！");
		}
		if(isValidCodeRight()){
			String username = jtfUser.getText();
			String password = jtfPass.getText();
			String userType = (String) jcbUserType.getSelectedItem();
			
			if(userType == "教务管理员"){userType = "0";}
			else if(userType == "教师"){userType = "1";}
			else if(userType == "学生"){userType = "2";}
			
			User user = userDao.login(username, password, userType);
			if(user != null){
				UserUtil.Sno =  user.getSno();
				UserUtil.Spassword =  user.getSpassword();
				UserUtil.Sname =  user.getSname();
				UserUtil.Ssex =  user.getSsex();
				UserUtil.Sage =  user.getSage();
				UserUtil.Major =  user.getMajor();
				UserUtil.userType = user.getUserType();
				System.out.println(UserUtil.userType);
				JOptionPane.showMessageDialog(null, "登陆成功！");
				super.setVisible(false);
				MainFrame mf = new MainFrame();
				Thread thread1=new Thread(mf);
				thread1.start();
			}else{
				JOptionPane.showMessageDialog(null, "用户名或者密码不正确！");
				jtfUser.setText("");
				jtfPass.setText("");
			}
		}
		
	}
	class MyListener extends WindowAdapter{
		public void windowClosing(WindowEvent e){		
			System.exit(0);
		}		
	}
	public static void main(String[] args) {
		new LoginFrame("xiaotian");
	}
	
	public boolean isValidCodeRight() {
		 
		if (jtfCode == null) {
			return false;
		}
		if (vcode == null) {
			return true;
		}
		if (vcode.getCode().equalsIgnoreCase(jtfCode.getText())) {
			return true;
		}
		return false;
	}
	
}
