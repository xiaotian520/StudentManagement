package com.student.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.student.dao.ScoreDao;
import com.student.model.sscore;



public class QueryScore extends JDialog implements ActionListener{
	private JTextField jtfCourseNo;
	private JButton jbtQuery;
	private JTable jTable1;
	DefaultTableModel tm ;
	ScoreDao scoreDao = new ScoreDao();
	public QueryScore(JFrame owner){
		super(owner,"成绩查询");
		this.setSize(350, 300);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel1 = new JPanel();
		jtfCourseNo = new JTextField(10);
		jbtQuery = new JButton("查询");
		jPanel1.add(new JLabel("课程号："));
		jPanel1.add(jtfCourseNo);
		jPanel1.add(jbtQuery);
		getContentPane().add(jPanel1, BorderLayout.NORTH);

		tm = new DefaultTableModel(new String[0][0] ,	new String[] { "学号", "姓名","成绩" });
		jTable1 = new JTable(tm){
			public boolean isCellEditable(int row, int column){return false;}
		};
		jTable1.getTableHeader().setReorderingAllowed(false);
		JScrollPane jScrollPane1 = new JScrollPane(jTable1);
		getContentPane().add(jScrollPane1, BorderLayout.CENTER);		
		

		jbtQuery.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		/*成绩查询*/
		if(e.getSource()==jbtQuery){
			if(jtfCourseNo.getText().length()==0){
				JOptionPane.showMessageDialog(null, "请输入课程号！");
				return ;
			}else{
				String CourseNo = jtfCourseNo.getText().trim();
				List<sscore> list = scoreDao.SearchCourse(CourseNo);
				if(list!=null){
					if(list.size()>0){
						tm.setRowCount(0);//清空表格
						for(int i=0;i<list.size();i++){
							String courseNo = list.get(i).getSname();
							String StudentNo = list.get(i).getSno();
							String grade = list.get(i).getGrade();
							tm.addRow(new String[]{StudentNo,courseNo,grade});
						}
					}else{
						JOptionPane.showMessageDialog(null, "课程号输入有误！");
					}
				}
			}
		}
	}
	
}
