package com.student.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.student.dao.UserDao;
import com.student.utils.UserUtil;

public class MyInfoDialog extends JDialog{

	private JTextField jtfSno;
	private JTextField jtfSpassword;
	private JTextField jtfSname;
	private ButtonGroup group;
	private JRadioButton jrbman;
	private JRadioButton jrbwom;
	private JTextField jtfSage;
	private JTextField jtfMajor;
	UserDao uDao = new UserDao();
	public MyInfoDialog(JFrame owner){
		super(owner,"个人信息");
		this.setSize(350, 250);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel1 = new JPanel();
		jtfSno = new JTextField(UserUtil.Sno,10);
		jtfSpassword = new JTextField(UserUtil.Spassword,10);
		
		jtfSname = new JTextField(UserUtil.Sname,10);
		group = new ButtonGroup();
		jrbman = new JRadioButton("  男    ");
		jrbwom = new JRadioButton("  女    ");
		group.add(jrbman);
		group.add(jrbwom);
		if(UserUtil.Ssex.trim()!=null){
			if("男".equals(UserUtil.Ssex.trim())){
				jrbman.setSelected(true);
				jrbwom.setEnabled(false);
			}else if("女".equals(UserUtil.Ssex.trim())){
				jrbwom.setSelected(true);
				jrbman.setEnabled(false);
			}
		}
		
		jtfSage = new JTextField(UserUtil.Sage,10);
		jtfMajor = new JTextField(UserUtil.Major,10);
		jtfSno.setEditable(false);
		jtfSpassword.setEditable(false);
		jtfSname.setEditable(false);
		jtfSage.setEditable(false);
		jtfMajor.setEditable(false);
		jPanel1.add(new JLabel("学号："));
		jPanel1.add(jtfSno);
		jPanel1.add(new JLabel("密码："));
		jPanel1.add(jtfSpassword);
		jPanel1.add(new JLabel("姓名："));
		jPanel1.add(jtfSname);
		jPanel1.add(new JLabel("性别："));
		jPanel1.add(jrbman);
		jPanel1.add(jrbwom);
		jPanel1.add(new JLabel("年龄："));
		jPanel1.add(jtfSage);
		jPanel1.add(new JLabel("专业："));
		jPanel1.add(jtfMajor);
		
		JPanel jpanel0 = new JPanel();
		JLabel jlaadd = new JLabel("个人信息查看");
		jlaadd.setFont(new java.awt.Font("DialogInput", 1, 24));
		jlaadd.setHorizontalAlignment(SwingConstants.CENTER);
		jlaadd.setForeground(Color.black);
		jpanel0.add(jlaadd);
		getContentPane().add(jpanel0, BorderLayout.NORTH);
		
		jPanel1.setLayout(new FlowLayout( FlowLayout.CENTER,3,20));
		getContentPane().add(jPanel1);
	
	}
	
}
