package com.student.view;

import java.awt.*;
import javax.swing.*;

public class AboutUs extends JDialog {

	public AboutUs (JFrame owner) {
		super(owner,"关于我们");
		this.setSize(600, 400);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		Container cp = this.getContentPane();
		JLabel jla_bg = new JLabel(new ImageIcon("images/bg.jpg", "学生成绩管理系统-关于我们"));
		jla_bg.setBounds(0, 0, 600, 400);
		cp.add(jla_bg);
		
	}
	public void paint(Graphics g) {// swing Graphics类绘图
		super.paint(g);

		
		g.setColor(new Color(255, 245, 225));
		g.setFont(new Font("华文行楷", Font.ITALIC, 50));
		g.drawString("谢谢使用!", 200, 130);

		g.setColor(new Color(255, 245, 225));
		g.setFont(new Font("华文行楷", Font.ITALIC, 50));
		g.drawString("学生成绩管理系统", 100, 200);
		
		g.setColor(new Color(255, 245, 225));
		g.setFont(new Font("华文楷体", Font.ITALIC, 30));// 署名
		g.drawString("作者:李易龙&唐兴旺", 300, 250);

		g.setColor(new Color(255, 255, 255));
		g.setFont(new Font("华文楷体", Font.ITALIC, 30));// 署名
		g.drawString("作者QQ:446691726", 180, 350);

	}

}
