package com.student.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import com.student.dao.CourseDao;
import com.student.model.Course;
import com.student.model.User;

public class CourseManage extends JDialog implements ActionListener {

	private JTextField sjtfCourse;//搜索课程
	private JTextField sjtfcNo;//搜索课程号
	private JTextField jtfCourse;//更新课程
	private JTextField jtfmajor;//更新专业
	private JTextField jtfcNo;//更新课程号
	private JTextField jtfteaNo;//更新教师编号
	private JButton jbtSelect;
	private JButton jbtadd;
	private JButton jbtupdate;
	private JButton jbtdelete;
	private JTable jTable1;
	private DefaultTableModel tm;
	private JScrollPane jScrollPane1;
	CourseDao cDao = new CourseDao();
	String cno = "",cname = "",teachno = "",cNo="",cName="";
	
	public CourseManage(JFrame owner) {
		super(owner,"开课目录");
		this.setSize(550, 700);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel0 = new JPanel();//添加面板
		JPanel jPanel1 = new JPanel();//表格面板
		JPanel jPanel2 = new JPanel();//更新面板
		JPanel jp1 = new JPanel();//更新子面板
		JPanel jp2 = new JPanel();//更新子面板
		Container cp = this.getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(jPanel0, BorderLayout.NORTH);
		cp.add(jPanel1, BorderLayout.CENTER);
		cp.add(jPanel2, BorderLayout.SOUTH);
		
		sjtfCourse = new JTextField(10);
		sjtfcNo = new JTextField(10);
		sjtfcNo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                // 第一种方法：判断输入
                char c = e.getKeyChar(); // 获取键盘输入的字符
                 if (Character.isDigit(c)) // 判断输入是否是数字
                 return; // true,返回
                 e.consume(); // false,消毁不匹配的输入
            }
        });
		jPanel0.add(new JLabel("课程编号："));
		jPanel0.add(sjtfcNo);
		jPanel0.add(new JLabel("开课课程："));
		jPanel0.add(sjtfCourse);
		jbtadd = new JButton("添加");
		jbtSelect = new JButton("查询");
		jPanel0.add(jbtSelect);
		jPanel0.add(jbtadd);
		
		tm = new DefaultTableModel(new String[0][0] ,new String[] { "课程号","课程名", "任课教师" });
		jTable1 = new JTable(tm){
			public boolean isCellEditable(int row, int column){return false;}
		};
		jTable1.getTableHeader().setReorderingAllowed(false);
		jScrollPane1 = new JScrollPane(jTable1);
		jPanel1.add(jScrollPane1, BorderLayout.CENTER);

		jPanel2.setLayout(new BorderLayout());
		jPanel2.add(jp1,BorderLayout.NORTH);
		jPanel2.add(jp2,BorderLayout.CENTER);
		jPanel2.setBorder(BorderFactory.createTitledBorder("课程信息更新"));
		jtfCourse = new JTextField(10);
		jtfcNo = new JTextField(10);
		jtfcNo.setEditable(false);
		jtfteaNo = new JTextField(10);
		jtfteaNo.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                // 第一种方法：判断输入
                char c = e.getKeyChar(); // 获取键盘输入的字符
                 if (Character.isDigit(c)) // 判断输入是否是数字
                 return; // true,返回
                 e.consume(); // false,消毁不匹配的输入
            }
        });
		jp1.add(new JLabel("课程编号："));
		jp1.add(jtfcNo);
		jp1.add(new JLabel("开课课程："));
		jp1.add(jtfCourse);
		jp2.setLayout(new FlowLayout(FlowLayout.CENTER,25,0));
		jp2.add(new JLabel("教师编号："));
		jp2.add(jtfteaNo);
		jbtupdate = new JButton("更新");
		jbtdelete = new JButton("删除");
		jp2.add(jbtupdate);
		jp2.add(jbtdelete);
		
		jbtadd.addActionListener(this);
		jbtSelect.addActionListener(this);
		jbtupdate.addActionListener(this);
		jbtdelete.addActionListener(this);
	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		cNo = sjtfcNo.getText().trim();
		cName = sjtfCourse.getText().trim();
		if(e.getSource()==jbtSelect){
			List<Course> list=cDao.SelectCourse(cNo,cName);
			if(list.size()>0){
				 tm.setRowCount(0);//清空表格
				 for(int i=0;i<list.size();i++){
					cno = list.get(i).getcNo();
					cname = list.get(i).getcName();
					teachno = list.get(i).getTeaNo();
					tm.addRow(new String[]{cno,cname,teachno});
				 	}
			}else{
					JOptionPane.showMessageDialog(null,"您输入的课程号或课程名不存在！");
				}
		}else if(e.getSource()==jbtadd){
			if(jTable1.getCellEditor() != null){
				jTable1.getCellEditor().stopCellEditing();//取消光标便于保存
			}
			if(cNo==null || "".equals(cNo)){
				JOptionPane.showMessageDialog(null, "请输入课程号！");
			}else if(cName==null || "".equals(cName)){
				JOptionPane.showMessageDialog(null, "请输入课程名！");
			}else{
				Course course = cDao.isExistCourse(cNo,cName);
				if(course != null){
					JOptionPane.showMessageDialog(null,"该课程号或课程已存在！");
				}else{
					if(cDao.AddCourse(cNo,cName)){
						JOptionPane.showMessageDialog(null,"课程添加成功！");
						cNo = null;cName=null;
						Select();
					}else{
						JOptionPane.showMessageDialog(null,"课程添加失败！");
					}
				}
			}
		}else if(e.getSource()==jbtupdate){
			String ucNo = jtfcNo.getText().trim();
			String ucName = jtfCourse.getText().trim();
			String uteaNo = jtfteaNo.getText().trim();
			if(ucNo==null || "".equals(ucName)){
				JOptionPane.showMessageDialog(null,"请选择要更新的课程信息");  
			}else{
				
				if(ucNo==null || "".equals(ucNo)){
					JOptionPane.showMessageDialog(null,"课程号不能为空！");
				}else if(ucName==null || "".equals(ucName)){
					JOptionPane.showMessageDialog(null,"课程名不能为空！");
				}else if(uteaNo==null || "".equals(uteaNo)){
					JOptionPane.showMessageDialog(null,"任课老师不能为空！");
				}else{
					if(cDao.UpdateCourse(ucNo,ucName,uteaNo))
					{
						cNo = null;cName=null;//将字段赋值为空
						Select();
						JOptionPane.showMessageDialog(null,"课程信息修改成功！");
						jtfcNo.setText("");
						jtfCourse.setText("");
						jtfteaNo.setText("");
					}else{
						JOptionPane.showMessageDialog(null,"课程信息更新失败！");
					}
				}
			}
			
			
		}else if(e.getSource()==jbtdelete){
			String dcNo = jtfcNo.getText().trim();
			int isDelete=0;
			if(dcNo==null || "".equals(dcNo)){
				JOptionPane.showMessageDialog(null,"请选择要删除的课程");  
			}else{
				//添加确认提示框，会返回一个整数
				isDelete = JOptionPane.showConfirmDialog(null, "确定删除这个课程？", "提示", JOptionPane.YES_NO_CANCEL_OPTION);
				//如果这个整数等于JOptionPane.YES_OPTION，则说明你点击的是“确定”按钮，则允许继续操作，否则结束
				if(isDelete == JOptionPane.YES_OPTION){
					if(cDao.DeleteCourse(dcNo)){
						Select();
						JOptionPane.showMessageDialog(null,"删除成功");
					}
				}
			}
		}
		
		/**表格监听*/
		jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
			String[] s = new String[3];
		public void mouseClicked(java.awt.event.MouseEvent e) {
			
			for(int i=0;i<s.length;i++){
				if(jTable1.getValueAt(jTable1.getSelectedRow(),i)!=null)
				{
					s[i] = (String) jTable1.getValueAt(jTable1.getSelectedRow(),i); //获取所选中的行的第i个位置的内容
				}
			}
				jtfcNo.setText(s[0]);
				jtfCourse.setText(s[1]);
				jtfteaNo.setText(s[2]);
				}
		}); 
		
		
	}
	
	public void Select(){
		List<Course> list=cDao.SelectCourse(cNo,cName);
		if(list.size()>0){
			 tm.setRowCount(0);//清空表格
			 for(int i=0;i<list.size();i++){
				cno = list.get(i).getcNo();
				cname = list.get(i).getcName();
				teachno = list.get(i).getTeaNo();
				tm.addRow(new String[]{cno,cname,teachno});
			 }
		}
	}
}
