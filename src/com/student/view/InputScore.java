package com.student.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.student.dao.ScoreDao;
import com.student.model.Course;
import com.student.model.Score;
import com.student.model.sscore;

public class InputScore extends JDialog implements ActionListener{
	
	private JTextField jtfCourseNo;
	private JButton jbtCancel;
	private JButton jbtSave;
	private JButton jbtInput;
	private JTable jTable1;
	private DefaultTableModel tm;
	ScoreDao scoreDao = new ScoreDao();
	List<sscore> list = null;
	String score = null;
	public InputScore(JFrame owner){
		super(owner,"成绩录入");
		this.setSize(350, 300);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel1 = new JPanel();
		jtfCourseNo = new JTextField(10);
		jbtInput = new JButton("开始录入");
		jPanel1.add(new JLabel("课程号："));
		jPanel1.add(jtfCourseNo);
		jPanel1.add(jbtInput);
		getContentPane().add(jPanel1, BorderLayout.NORTH);

		JPanel jPanel2 = new JPanel();
		jbtSave = new JButton("保存");
		jbtCancel = new JButton("取消");
		jPanel2.add(jbtSave);
		jPanel2.add(jbtCancel);		
		getContentPane().add(jPanel2, BorderLayout.SOUTH);

		tm = new DefaultTableModel(new String[0][0] ,	new String[] { "学号", "姓名","成绩" });
		jTable1 = new JTable(tm){
			public boolean isCellEditable(int row, int column){
				if(column == 2){
				       return true;
				    }else{
				       return false;
				    }
			}
		};
		jTable1.getTableHeader().setReorderingAllowed(false);//设置表头不可移动
		JScrollPane jScrollPane1 = new JScrollPane(jTable1);
		getContentPane().add(jScrollPane1, BorderLayout.CENTER);		
		
		jbtSave.setEnabled(false);
		jbtInput.addActionListener(this);
		jbtSave.addActionListener(this);
		jbtCancel.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbtInput){
			//成绩录入
			if(jtfCourseNo.getText().length()==0){
				JOptionPane.showMessageDialog(null, "请输入课程号！");
				jbtSave.setEnabled(false);
				return ;
			}else{
				String CourseNo = jtfCourseNo.getText().trim();
				list = scoreDao.SearchCourse(CourseNo);
				if(list!=null){
					if(list.size()>0){
						tm.setRowCount(0);//清空表格
						for(int i=0;i<list.size();i++){
							String courseNo = list.get(i).getSname();
							String StudentNo = list.get(i).getSno();
							String grade = list.get(i).getGrade();
							tm.addRow(new String[]{StudentNo,courseNo,grade});
						}
					}else{
						JOptionPane.showMessageDialog(null, "课程号输入有误！");
						jbtSave.setEnabled(false);
					}
					jbtSave.setEnabled(true);
				}
			}
		}else if(e.getSource()==jbtSave){
			//成绩保存
			boolean flag = false;
			String score =null;
			if(jTable1.getCellEditor() != null){
				jTable1.getCellEditor().stopCellEditing();//取消光标便于保存
			}
			
			if(list!=null){
				if(list.size()>0){
					for(int i=0;i<list.size();i++){
						String courseNo = jtfCourseNo.getText().trim();
						String StudentNo = (String) tm.getValueAt(i, 0);
						score = (String) tm.getValueAt(i, 2);
						if(courseNo!=null || "".equals(courseNo)){
							String regex = "^\\d{1,3}$";
							if(!score.matches(regex)){
								JOptionPane.showMessageDialog(null, "请检查成绩是否填写正确");
								return;
							}else{
								flag = scoreDao.updateScore(courseNo,StudentNo,score);
							}
						}else{
							JOptionPane.showMessageDialog(null, "请输入学生成绩！");
						}
					}
					
				}
			}
			if(flag==true){
				JOptionPane.showMessageDialog(null, "成绩保存成功！");
			}else{
				JOptionPane.showMessageDialog(null, "成绩保存失败！");
			}
		}else if(e.getSource()==jbtCancel){
			this.dispose();
		}
		
	}
	
}
