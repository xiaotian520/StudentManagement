package com.student.view;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.student.dao.ScoreDao;
import com.student.model.Score;
import com.student.model.sscore;


public class StudentQueryScore extends JDialog implements ActionListener{

	private JTextField jtfStudentNo;
	private JButton jbtSearch;
	private JTable jTable1;
	private JScrollPane jScrollPane1;
	private DefaultTableModel tm;
	ScoreDao scoreDao = new ScoreDao();
	
	public StudentQueryScore(JFrame owner){
		super(owner,"成绩查询");
		this.setSize(300, 300);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel1 = new JPanel();
		jPanel1.setLayout(new BorderLayout());
		JPanel jPanel2 = new JPanel();
		jtfStudentNo = new JTextField("201809",10);
		jbtSearch = new JButton("查询");
		jPanel2.add(new JLabel("学  号："));
		jPanel2.add(jtfStudentNo);
		jPanel2.add(jbtSearch);
		jPanel1.add(jPanel2, BorderLayout.NORTH);
		getContentPane().add(jPanel1, BorderLayout.CENTER);
	
		tm = new DefaultTableModel(new String[0][0] ,new String[] { "学号","课程名", "成绩" });
		jTable1 = new JTable(tm){
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		jTable1.getTableHeader().setReorderingAllowed(false);
		jScrollPane1 = new JScrollPane(jTable1);
		jPanel1.add(jScrollPane1, BorderLayout.CENTER);

		jbtSearch.setEnabled(true);
		jbtSearch.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbtSearch){
			
			String sno = jtfStudentNo.getText().trim();
			
			if(sno.length()==0){
				JOptionPane.showMessageDialog(null, "请输入学号！");
				return;
			}else{
				List<Score> list = scoreDao.SearchSno(sno);//通过学号查询成绩
				System.out.println(list);
				if(list!=null){
					if(list.size()>0){
						tm.setRowCount(0);//清空表格
						for(int i=0;i<list.size();i++){
							String StudentNo = list.get(i).getsNo();
							String courseNo = list.get(i).getcNo();
							String grade = list.get(i).getGrade();
							tm.addRow(new String[]{StudentNo,courseNo,grade});
						}
					}else{
						JOptionPane.showMessageDialog(null, "学号输入有误！");
					}
				}
			}
			
		}
	}

}
