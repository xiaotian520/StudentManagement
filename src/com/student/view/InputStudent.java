package com.student.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.student.dao.UserDao;
import com.student.model.User;

public class InputStudent extends JDialog implements ActionListener{

	private JTextField jtfSno;
	private JTextField jtfSpassword;
	private JTextField jtfSname;
	private ButtonGroup group;
	private JRadioButton jrbman;
	private JRadioButton jrbwom;
	private JTextField jtfSage;
	private JTextField jtfMajor;
	private JButton jbtCancel;
	private JButton jbtadd;
	private JTable jTable1;
	UserDao uDao = new UserDao();
	public InputStudent(JFrame owner){
		super(owner,"添加学生");
		this.setSize(350, 250);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		JPanel jPanel1 = new JPanel();
		jtfSno = new JTextField("201809",10);
		jtfSno.addKeyListener(new KeyAdapter() {
	            public void keyTyped(KeyEvent e) {
	                // 第一种方法：判断输入
	                char c = e.getKeyChar(); // 获取键盘输入的字符
	                 if (Character.isDigit(c)) // 判断输入是否是数字
	                 return; // true,返回
	                 e.consume(); // false,消毁不匹配的输入
	            }
	        });
		jtfSpassword = new JTextField("默认学号后六位",10);
		jtfSpassword.setEditable(false);
		jtfSname = new JTextField(10);
		group = new ButtonGroup();
		jrbman = new JRadioButton("  男    ");
		jrbwom = new JRadioButton("  女    ");
		group.add(jrbman);
		group.add(jrbwom);
		jtfSage = new JTextField(10);
		jtfSage.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                // 第一种方法：判断输入
                char c = e.getKeyChar(); // 获取键盘输入的字符
                 if (Character.isDigit(c)) // 判断输入是否是数字
                 return; // true,返回
                 e.consume(); // false,消毁不匹配的输入
            }
        });
		jtfMajor = new JTextField("软件技术",10);
		jPanel1.add(new JLabel("学号："));
		jPanel1.add(jtfSno);
		jPanel1.add(new JLabel("密码："));
		jPanel1.add(jtfSpassword);
		jPanel1.add(new JLabel("姓名："));
		jPanel1.add(jtfSname);
		jPanel1.add(new JLabel("性别："));
		jPanel1.add(jrbman);
		jPanel1.add(jrbwom);
		jPanel1.add(new JLabel("年龄："));
		jPanel1.add(jtfSage);
		jPanel1.add(new JLabel("专业："));
		jPanel1.add(jtfMajor);
		
		JPanel jpanel0 = new JPanel();
		JLabel jlaadd = new JLabel("学生信息添加");
		jlaadd.setFont(new java.awt.Font("DialogInput", 1, 24));
		jlaadd.setHorizontalAlignment(SwingConstants.CENTER);
		jlaadd.setForeground(Color.black);
		jpanel0.add(jlaadd);
		getContentPane().add(jpanel0, BorderLayout.NORTH);
		
		jPanel1.setLayout(new FlowLayout());
		getContentPane().add(jPanel1);

		JPanel jPanel2 = new JPanel();
		jbtadd = new JButton("添加");
		jbtCancel = new JButton("取消");
		jPanel2.add(jbtadd);
		jPanel2.add(jbtCancel);		
		getContentPane().add(jPanel2, BorderLayout.SOUTH);
		jbtadd.addActionListener(this);
		jbtCancel.addActionListener(this);
	
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbtadd){
			String sno = jtfSno.getText().trim();//学号
			String sname = jtfSname.getText().trim();//姓名
			String sex = "";//性别
			if(jrbman.isSelected()){
				sex = jrbman.getText();
			}else if(jrbwom.isSelected()){
				sex = jrbwom.getText();
			}
			String sage = jtfSage.getText().trim();//年龄
			String major = jtfMajor.getText().trim();//专业
			User user = uDao.isExistUser(sno);
			if(user != null){
				JOptionPane.showMessageDialog(null,"该学生已存在！");
			}else if(sno==null || "".equals(sno)){
				JOptionPane.showMessageDialog(null,"学号不能为空！");
			}else if(!sno.matches("201809\\d{4}")){
			    JOptionPane.showMessageDialog(null,"学号填写错误！");
			}else if(sex==null || "".equals(sex)){
				JOptionPane.showMessageDialog(null,"请选择学生性别！");
			}else if(sname==null || "".equals(sname)){
				JOptionPane.showMessageDialog(null,"学生姓名不能为空！");
			}else if(sage==null|| "".equals(sage)){
				JOptionPane.showMessageDialog(null,"学生年龄不能为空！");
			}else if(!sage.matches("[0-9]{2}")){
				JOptionPane.showMessageDialog(null,"学生年龄输入有误！");
			}else if(major==null|| "".equals(major)){
				JOptionPane.showMessageDialog(null,"学生专业不能为空！");
			}else{
				String pass = sno.substring(sno.length()-6, sno.length());
				jtfSpassword.setText(pass);
				if(uDao.AddStudent(sno,pass,sname,sex,sage,major)){
					JOptionPane.showMessageDialog(null,"学生添加成功！");
					jtfSno.setText("");
					jtfSpassword.setText("");
					jtfSname.setText("");
					group.clearSelection();
					jtfSage.setText("");
					jtfMajor.setText("");
				}else{
					JOptionPane.showMessageDialog(null,"学生添加失败！");
				}
			}
			
			
		}else if(e.getSource()==jbtCancel){
			this.dispose();
		}
		
	}
}
