package com.student.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.student.dao.UserDao;
import com.student.model.User;
import com.student.utils.UserUtil;

public class UpdatePassword extends JDialog implements ActionListener {

	private JTextField jtfUser;
	private JTextField jtfOldPass;
	private JTextField jtfNewPass;
	private JButton jbtSave;
	private JButton jbtCancel;
	UserDao uDao = new UserDao();
	String sno = "", spass = "", sname = "", ssex = "", sage = "", major = "";
	
	public UpdatePassword(JFrame owner) {
		super(owner,"密码修改");
		this.setSize(250, 180);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		this.setTitle("登录");
		JPanel  p1 = new JPanel();
		JPanel  p2 = new JPanel();
		Container cp = this.getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(p1,BorderLayout.CENTER);
		cp.add(p2,BorderLayout.SOUTH);
		p1.setLayout(new FlowLayout(FlowLayout.LEFT,20,10));
		p1.add(new JLabel("用户名："));
		jtfUser = new JTextField(UserUtil.Sno,10);
		p1.add(jtfUser);
		p1.add(new JLabel("旧密码："));
		jtfOldPass = new JTextField(10);
		p1.add(jtfOldPass);
		p1.add(new JLabel("新密码："));
		jtfNewPass = new JTextField(10);
		p1.add(jtfNewPass);
		jbtSave = new JButton("保存");
		jbtCancel = new JButton("取消");
		p2.add(jbtSave);
		p2.add(jbtCancel);
		
		jbtSave.addActionListener(this);
		jbtCancel.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbtSave){
			String sno = jtfUser.getText().trim();//获取输入的学号
			String opass = jtfOldPass.getText().trim();//获取输入的旧密码
			String npass = jtfNewPass.getText().trim();//获取输入的新密码
			if(sno==null || "".equals(sno)){
				JOptionPane.showMessageDialog(null,"用户名不能为空！");
			}else if(opass==null || "".equals(opass)){
				JOptionPane.showMessageDialog(null,"旧密码不能为空！");
			}else if(npass==null || "".equals(npass)){
				JOptionPane.showMessageDialog(null,"新密码不能为空！");
			}else{
				List<User> list = uDao.SelectStudent(sno);
				if(list.size()>0){
					String pass = list.get(0).getSpassword().trim();
					if(!pass.equals(opass)){
						JOptionPane.showMessageDialog(null,"旧密码输入错误！");
					}else{
						if(!npass.matches("[\\w]{6,12}")){
							JOptionPane.showMessageDialog(null,"新密码长度为6-12位字母或数字组合！");
						}else{
							if(uDao.Updatepassword(sno,npass)){
								jtfUser.setText("");
								jtfOldPass.setText("");
								jtfNewPass.setText("");
								JOptionPane.showMessageDialog(null,"保存成功，成功更新密码！");
							}else{
								JOptionPane.showMessageDialog(null,"未知错误，更新密码失败！");
							}
						}
					}
					
				}else{
					JOptionPane.showMessageDialog(null,"您输入的用户名不存在！");
				}
			}
		}else if(e.getSource()==jbtCancel){
			this.dispose();
		}
		
	}

}
