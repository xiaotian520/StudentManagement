package com.student.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import com.student.dao.CourseDao;
import com.student.dao.ScoreDao;
import com.student.dao.UserDao;
import com.student.model.Course;

public class Coursecatalogue extends JDialog implements ActionListener{
	
	private JButton jbtSelectcourse;
	private JTextField jlamajor;
	private JButton jbtSave;
	private JButton jbtCacel;
	private JTable jTable1;
	private DefaultTableModel tm ;
	private JComboBox jcbcourse;
	private JComboBox jcbmajor;
	UserDao uDao = new UserDao();
	CourseDao cDao = new CourseDao();
	
	ScoreDao scoreDao = new ScoreDao();
	public Coursecatalogue(JFrame owner){//依附的窗体 JFrame
		super(owner,"学生课程管理");
		this.setSize(350, 300);
		this.setLocationRelativeTo(null);//设置窗体居中显示
		this.setResizable(false);//设置窗体不可改变大小
		this.setModal(true);//设置模态框
		Container cp = this.getContentPane();
		cp.setLayout(new FlowLayout(FlowLayout.CENTER,0,20));
		JPanel jPanel0 = new JPanel();
		JPanel jPanel1 = new JPanel();
		JPanel jPanel3 = new JPanel();
		cp.add(jPanel0, BorderLayout.NORTH);
		cp.add(jPanel1, BorderLayout.CENTER);
		cp.add(jPanel3, BorderLayout.SOUTH);
		
		JLabel jlatext = new JLabel("专业课程设置");
		jlatext.setFont(new java.awt.Font("DialogInput", 1, 24));
		jlatext.setHorizontalAlignment(SwingConstants.CENTER);
		jlatext.setForeground(Color.black);
		jPanel0.add(jlatext);
		
		jPanel1.setLayout(new BorderLayout());
		JPanel jp1 = new JPanel();
		JPanel jp2 = new JPanel();
		jPanel1.add(jp1, BorderLayout.NORTH);
		jPanel1.add(jp2, BorderLayout.SOUTH);
		JLabel jlamajor = new JLabel("专业名:");
		jcbmajor = new JComboBox();
		jcbmajor.addItem("请选择专业");
		jcbmajor.addItem("软件技术");
		jcbmajor.addItem("信息安全技术");
		jcbmajor.addItem("电子商务");
		jcbmajor.addItem("商务英语");
		jcbmajor.addItem("市场营销");
		jcbmajor.addItem("新能源汽车");
		jcbmajor.addItem("会计");
		jcbmajor.addItem("物流管理");
		jcbmajor.addItem("工程造价");
		jcbmajor.addItem("移动应用开发");
		jp1.add(jlamajor);
		jp1.add(jcbmajor);
		
		JLabel jlacourse = new JLabel("开课课程:");
		jcbcourse = new JComboBox();
		jcbcourse.addItem("请选择课程");
		jbtSelectcourse = new JButton("查询可选课程");
		jp2.add(jlacourse);
		jp2.add(jcbcourse);
		jp2.add(jbtSelectcourse);
		
		jbtSave = new JButton("设置");
		jbtCacel = new JButton("取消");
		jPanel3.add(jbtSave);
		jPanel3.add(jbtCacel);
		

		jbtSelectcourse.addActionListener(this);
		jbtSave.addActionListener(this);
		jbtCacel.addActionListener(this);

	}

	
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==jbtSelectcourse){
			jcbcourse.removeAllItems();
			jcbcourse.addItem("请选择课程");
			List<Course> list = cDao.SelectCourse(null, null);
			for(int i = 0; i< list.size();i++){
				String cName =  list.get(i).getcName();
				jcbcourse.addItem(cName);
			}
		}else if(e.getSource()==jbtSave){
			String major = (String) jcbmajor.getSelectedItem();
			String course = (String) jcbcourse.getSelectedItem();
			if("请选择专业".equals(major)){
				JOptionPane.showMessageDialog(null,"请选择专业！");
			}else if("请选择课程".equals(course)){
				JOptionPane.showMessageDialog(null,"请先查询课程并选择课程！");
			}else{
				if(cDao.addStudentCourse(course, major)){
					JOptionPane.showMessageDialog(null,"设置成功，请勿重复设置！");
				}else{
					JOptionPane.showMessageDialog(null,"设置失败！");
				}
			}
		}else if(e.getSource()==jbtCacel){//取消 销毁窗体
			this.dispose();
		}
	}
	
	
	
}
