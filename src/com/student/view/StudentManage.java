package com.student.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import com.student.dao.UserDao;
import com.student.model.User;

public class StudentManage extends JDialog implements ActionListener{

	private JButton jbSelect;
	private JTextField S_jtfSno;
	private JTextField jtfSno;
	private JTextField jtfSpassword;
	private JTextField jtfSname;
	private ButtonGroup group;
	private JRadioButton jrbman;
	private JRadioButton jrbwom;
	private JTextField jtfSage;
	private JComboBox jcbmajor;
	private JButton jbtdelete;
	private JButton jbtupdate;
	private JTable jTable1;
	private DefaultTableModel tm;
	String sno = "", spass = "", sname = "", ssex = "", sage = "", major = "";
	UserDao uDao = new UserDao();
	public StudentManage(JFrame owner){
		super(owner,"添加学生");
		this.setSize(500, 700);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setModal(true);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		Container cp = this.getContentPane();
		cp.setLayout(new FlowLayout());
		JPanel jp_search = new JPanel();
		JPanel jp_table = new JPanel();
		JPanel jp_form = new JPanel();
		JPanel jp_button = new JPanel();
		
		cp.add(jp_search,BorderLayout.NORTH);
		cp.add(jp_table, BorderLayout.CENTER);
		cp.add(jp_form,BorderLayout.SOUTH);
		cp.add(jp_button,BorderLayout.SOUTH);
		JLabel S_jlaSno = new JLabel("学号：");
		S_jtfSno = new JTextField("201809",10);
		S_jtfSno.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                // 第一种方法：判断输入
                char c = e.getKeyChar(); // 获取键盘输入的字符
                 if (Character.isDigit(c)) // 判断输入是否是数字
                 return; // true,返回
                 e.consume(); // false,消毁不匹配的输入
            }
        });
		jbSelect = new JButton("查询");
		
		String[] strs = {"学号","密码","姓名","性别","年龄","专业"};
		tm = new DefaultTableModel(strs, 0){
			   public boolean isCellEditable(int row, int column) {
			    return false;		//返回true表示能编辑，false表示不能编辑
			   }   
			  };
		jTable1 = new JTable(tm);
		jTable1.getTableHeader().setReorderingAllowed(false);
		JScrollPane jsp = new JScrollPane(jTable1);
		
		JLabel jlaSno = new JLabel("学号：");
		jtfSno = new JTextField(8);
		JLabel jlaSpassword = new JLabel("密码：");
		jtfSpassword = new JTextField(8);
		JLabel jlaSname = new JLabel("姓名：");
		jtfSname = new JTextField(8);
		JLabel jlaSsex = new JLabel("性别：");
		jrbman = new JRadioButton(" 男 ");
		jrbwom = new JRadioButton(" 女 ");
		group = new ButtonGroup();
		group.add(jrbman);
		group.add(jrbwom);
		JLabel jlaSage = new JLabel("年龄：");
		jtfSage = new JTextField(8);
		JLabel jlaMajor = new JLabel("专业：");
		jcbmajor = new JComboBox();
		jcbmajor.addItem("请选择专业");
		jcbmajor.addItem("软件技术");
		jcbmajor.addItem("信息安全技术");
		jcbmajor.addItem("电子商务");
		jcbmajor.addItem("商务英语");
		jcbmajor.addItem("市场营销");
		jcbmajor.addItem("新能源汽车");
		jcbmajor.addItem("会计");
		jcbmajor.addItem("物流管理");
		jcbmajor.addItem("工程造价");
		jcbmajor.addItem("移动应用开发");
		JPanel jpanel1 = new JPanel();
		JPanel jpanel2 = new JPanel();
		
		
		jp_search.add(S_jlaSno);
		jp_search.add(S_jtfSno);
		jp_search.add(jbSelect);
		jp_table.add(jsp,BorderLayout.CENTER);
		jp_form.setLayout(new BorderLayout());
		jp_form.setBorder(BorderFactory.createTitledBorder("更新学生信息"));
		jp_form.add(jpanel1,BorderLayout.NORTH);
		jp_form.add(jpanel2,BorderLayout.CENTER);
		
		jpanel1.add(jlaSno);
		jpanel1.add(jtfSno);
		jtfSno.setEditable(false);
		jpanel1.add(jlaSpassword);
		jpanel1.add(jtfSpassword);
		jtfSpassword.setEditable(false);
		jpanel1.add(jlaSname);
		jpanel1.add(jtfSname);
		jpanel2.add(jlaSsex);
		jpanel2.add(jrbman);
		jpanel2.add(jrbwom);
		jpanel2.add(jlaSage);
		jpanel2.add(jtfSage);
		jpanel2.add(jlaMajor);
		jpanel2.add(jcbmajor);
		
		jbtupdate = new JButton("更新");
		jbtdelete = new JButton("删除");
		jp_button.add(jbtupdate);
		jp_button.add(jbtdelete);
		
		jbSelect.addActionListener(this);
		jbtupdate.addActionListener(this);
		jbtdelete.addActionListener(this);
	
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbSelect){//查询
			String sno = S_jtfSno.getText().trim();//学号
			List<User> list = uDao.SelectStudent(sno);
			if(list.size()>0){
				 tm.setRowCount(0);//清空表格
				 for(int i=0;i<list.size();i++){
					 sno = list.get(i).getSno();
					 spass = list.get(i).getSpassword();
					 sname = list.get(i).getSname();
					 ssex = list.get(i).getSsex();
					 sage = list.get(i).getSage();
					 major = list.get(i).getMajor();
					 tm.addRow(new String[]{sno,spass,sname,ssex,sage,major});
				 	}
			}else{
				JOptionPane.showMessageDialog(null,"您输入的学号不存在！");
			}
		}else if(e.getSource()==jbtupdate){//更新
			String sno = jtfSno.getText().trim();
			if(sno==null || "".equals(sno)){
				JOptionPane.showMessageDialog(null,"请选择要更新的学生信息");  
			}else{
				String spass = jtfSpassword.getText().trim();
				String sname = jtfSname.getText().trim();
				String ssex = "";//作者性别
				if(jrbman.isSelected()){
					ssex = jrbman.getText();
				}else if(jrbwom.isSelected()){
					ssex = jrbwom.getText();
				}
				String sage = jtfSage.getText().trim();
				String major = (String) jcbmajor.getSelectedItem();
				if(sno==null || "".equals(sno)){
					JOptionPane.showMessageDialog(null,"学号不能为空！");
				}else if(ssex==null || "".equals(ssex)){
					JOptionPane.showMessageDialog(null,"请选择学生性别！");
				}else if(sname==null || "".equals(sname)){
					JOptionPane.showMessageDialog(null,"学生姓名不能为空！");
				}else if(sage==null|| "".equals(sage)){
					JOptionPane.showMessageDialog(null,"学生年龄不能为空！");
				}else if("请选择专业".equals(major)){
					JOptionPane.showMessageDialog(null,"请选择专业！");
				}else{
					if(uDao.UpdateStudent(sno,spass,sname,ssex,sage,major))
					{
						select();
						JOptionPane.showMessageDialog(null,"学生信息修改成功！");
						jtfSno.setText("");
						jtfSpassword.setText("");
						jtfSname.setText("");
						group.clearSelection();
						jtfSage.setText("");
						group.clearSelection();
						jcbmajor.setSelectedItem("请选择专业");
					}else{
						JOptionPane.showMessageDialog(null,"学生信息更新失败！");
					}
				}
			}
			
		}else if(e.getSource()==jbtdelete){//删除
			String sno = jtfSno.getText().trim();
			int isDelete=0;
			if(sno==null || "".equals(sno)){
				JOptionPane.showMessageDialog(null,"请选择要删除的学生");  
			}else{
				//添加确认提示框，会返回一个整数
				isDelete = JOptionPane.showConfirmDialog(null, "确定删除这个学生？", "提示", JOptionPane.YES_NO_CANCEL_OPTION);
				//如果这个整数等于JOptionPane.YES_OPTION，则说明你点击的是“确定”按钮，则允许继续操作，否则结束
				if(isDelete == JOptionPane.YES_OPTION){
					if(uDao.DeleteStudent(sno)){
						select();
						JOptionPane.showMessageDialog(null,"删除成功");
					}
				}
			}
		}
		
		
		
		/**表格监听*/
		jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
			String[] s = new String[6];
		public void mouseClicked(java.awt.event.MouseEvent e) {
			
			for(int i=0;i<s.length;i++){
				if(jTable1.getValueAt(jTable1.getSelectedRow(),i)!=null)
				{
					s[i] = (String) jTable1.getValueAt(jTable1.getSelectedRow(),i); //获取所选中的行的第i个位置的内容
				}
			}
				jtfSno.setText(s[0]);
				jtfSpassword.setText(s[1]);
				jtfSname.setText(s[2]);
				//性别
				String sex = s[3].trim();
				if("男".equals(sex)){
					jrbman.setSelected(true);
				}else if("女".equals(sex)){
					jrbwom.setSelected(true);
				}
				jtfSage.setText(s[4]);
				//类别
				jcbmajor.setSelectedItem(s[5]);
				}
		}); 
		
	}


	private void select() {
		String sno = S_jtfSno.getText().trim();//学号
		List<User> list = uDao.SelectStudent(sno);
		if(list.size()>0){
			 tm.setRowCount(0);//清空表格
			 for(int i=0;i<list.size();i++){
				 sno = list.get(i).getSno();
				 spass = list.get(i).getSpassword();
				 sname = list.get(i).getSname();
				 ssex = list.get(i).getSsex();
				 sage = list.get(i).getSage();
				 major = list.get(i).getMajor();
				 tm.addRow(new String[]{sno,spass,sname,ssex,sage,major});
			 }
		}
	}
}
