package com.student.model;

public class Course {
	
	private String cNo;//课程号
	private String cName;//课程名
	private String teaNo;//教师编号
	
	
	public String getcNo() {
		return cNo;
	}
	public void setcNo(String cNo) {
		this.cNo = cNo;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getTeaNo() {
		return teaNo;
	}
	public void setTeaNo(String teaNo) {
		this.teaNo = teaNo;
	}
	@Override
	public String toString() {
		return "Course [cNo=" + cNo + ", cName=" + cName + ", teaNo=" + teaNo + "]";
	}

}
