package com.student.model;

public class User {

	private String Sno ;//  账号  / 学号
	private String Spassword ;//密码
	private String Sname ;//姓名
	private String Ssex ;//性别
	private String Sage;//年龄
	private String Major ;//专业
	private String userType;//用户类型 
	private Score score;
	
	public Score getScore() {
		return score;
	}
	public void setScore(Score score) {
		this.score = score;
	}
	public String getSno() {
		return Sno;
	}
	public void setSno(String sno) {
		Sno = sno;
	}
	public String getSpassword() {
		return Spassword;
	}
	public void setSpassword(String spassword) {
		Spassword = spassword;
	}
	public String getSname() {
		return Sname;
	}
	public void setSname(String sname) {
		Sname = sname;
	}
	public String getSsex() {
		return Ssex;
	}
	public void setSsex(String ssex) {
		Ssex = ssex;
	}
	public String getMajor() {
		return Major;
	}
	public void setMajor(String major) {
		Major = major;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	public String getSage() {
		return Sage;
	}
	public void setSage(String sage) {
		Sage = sage;
	}
	@Override
	public String toString() {
		return "User [Sno=" + Sno + ", Spassword=" + Spassword + ", Sname=" + Sname + ", Ssex=" + Ssex + ", Sage="
				+ Sage + ", Major=" + Major + ", userType=" + userType + ", score=" + score + "]";
	}
	
	
}
