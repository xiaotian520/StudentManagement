package com.student.utils;
/**
 * 数据库操作工具类
 * 从properties中取数据
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class DBUtils {

	//设置MySql连接属性
	private static String url = null;
	private static String user = null;
	private static String password = null;
	private static String driverClass = null;
	
	static {
		//注册驱动
		try {
			//从db.properties中取数据赋值给静态变量
			/**
			 * 使用ResourceBundle读取.properties中的数据
			 * ResourceBundle 资源文件夹
			 */
			ResourceBundle rb = ResourceBundle.getBundle("db");//不用写后缀名
			url = rb.getString("url");
			user = rb.getString("user");
			password = rb.getString("password");
			driverClass = rb.getString("driverClass");
			Class.forName(driverClass);//加载驱动
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 返回一个数据库连接
	 * @return
	 * @throws SQLException 
	 */
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
	
	/**
	 * 关闭连接
	 * @param rs
	 * @param stmt
	 * @param conn
	 */
	public static void close(ResultSet rs , Statement stmt , Connection conn) {
		
		//关闭资源
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
