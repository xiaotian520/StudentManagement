# StudentManagement

#### 介绍
基于Swing的学生管理系统

#### 软件架构
```
大学生期末作业，基于Java图形化界面Swing，
分为教师，学生，管理员三种权限，
自己太菜了，有些地方还有些小毛病，未来得及修改，
新手可以参考学习。
```

#### 安装教程

1.  将项目克隆下来，，将doc文件夹下的sql文件导入本地数据并修改db.properties的数据库信息
2.  运行loginFrame文件
3.  运行成功界面

![avatar](doc/登录界面.png)

4.  管理员登录成功界面

![avatar](doc/首页.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
