/*
Navicat MySQL Data Transfer

Source Server         : xiaotian
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : studentmanager

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-12-24 15:44:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `cNo` varchar(10) NOT NULL,
  `cName` varchar(45) NOT NULL,
  `teaNo` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`cNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('001', 'JAVA高级', '1001');
INSERT INTO `course` VALUES ('0012', 'JAVA高级1', '1001');
INSERT INTO `course` VALUES ('002', 'JavaWeb', '1002');
INSERT INTO `course` VALUES ('003', 'BootStrap', '1003');
INSERT INTO `course` VALUES ('004', 'C#', '1004');
INSERT INTO `course` VALUES ('005', 'C语言', '1005');
INSERT INTO `course` VALUES ('006', 'C++', '1006');
INSERT INTO `course` VALUES ('007', 'UML建模', '1007');
INSERT INTO `course` VALUES ('008', 'JavaScript', '1008');
INSERT INTO `course` VALUES ('009', 'PhotoShop', '1009');
INSERT INTO `course` VALUES ('010', '形式与政策123', '1010');
INSERT INTO `course` VALUES ('015', 'MySql', '1001');
INSERT INTO `course` VALUES ('022', 'abc', '1001');

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sNo` varchar(19) NOT NULL,
  `cNo` varchar(10) NOT NULL,
  `grade` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `score_cno` (`cNo`),
  KEY `score_sno` (`sNo`),
  CONSTRAINT `score_cno` FOREIGN KEY (`cNo`) REFERENCES `course` (`cNo`),
  CONSTRAINT `score_sno` FOREIGN KEY (`sNo`) REFERENCES `t_user` (`Sno`)
) ENGINE=InnoDB AUTO_INCREMENT=10085 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('10001', '2018090002', '001', '123');
INSERT INTO `score` VALUES ('10002', '2018090002', '002', '10');
INSERT INTO `score` VALUES ('10003', '2018090002', '003', '');
INSERT INTO `score` VALUES ('10004', '2018090002', '004', null);
INSERT INTO `score` VALUES ('10005', '2018090003', '001', '100');
INSERT INTO `score` VALUES ('10006', '2018090004', '001', '100');
INSERT INTO `score` VALUES ('10052', '2018090001', '0012', null);
INSERT INTO `score` VALUES ('10053', '2018090010', '0012', null);
INSERT INTO `score` VALUES ('10054', '2018090011', '0012', null);
INSERT INTO `score` VALUES ('10055', '2018090012', '0012', null);
INSERT INTO `score` VALUES ('10056', '2018090014', '0012', null);
INSERT INTO `score` VALUES ('10059', '1001', '001', null);
INSERT INTO `score` VALUES ('10061', '2018090004', '001', '100');
INSERT INTO `score` VALUES ('10062', '2018090005', '001', '80');
INSERT INTO `score` VALUES ('10063', '2018090006', '001', '80');
INSERT INTO `score` VALUES ('10064', '2018090007', '001', '80');
INSERT INTO `score` VALUES ('10065', '2018090013', '001', '82');
INSERT INTO `score` VALUES ('10066', '2018090015', '001', '80');
INSERT INTO `score` VALUES ('10067', '2018090030', '001', '80');
INSERT INTO `score` VALUES ('10068', '2018090033', '001', '80');
INSERT INTO `score` VALUES ('10069', '2018090035', '001', '80');
INSERT INTO `score` VALUES ('10074', '1001', '002', null);
INSERT INTO `score` VALUES ('10075', '2018090003', '002', null);
INSERT INTO `score` VALUES ('10076', '2018090004', '002', null);
INSERT INTO `score` VALUES ('10078', '2018090006', '002', null);
INSERT INTO `score` VALUES ('10079', '2018090007', '002', null);
INSERT INTO `score` VALUES ('10080', '2018090013', '002', null);
INSERT INTO `score` VALUES ('10081', '2018090015', '002', null);
INSERT INTO `score` VALUES ('10082', '2018090030', '002', null);
INSERT INTO `score` VALUES ('10083', '2018090033', '002', null);
INSERT INTO `score` VALUES ('10084', '2018090035', '002', null);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `Sno` varchar(10) NOT NULL DEFAULT '2018090003',
  `Spassword` varchar(10) NOT NULL DEFAULT '09000',
  `Sname` varchar(10) NOT NULL DEFAULT 'xiaotian',
  `Ssex` varchar(3) NOT NULL DEFAULT '男',
  `Sage` varchar(3) NOT NULL DEFAULT '21',
  `major` varchar(20) NOT NULL DEFAULT '软件技术',
  `userType` varchar(1) NOT NULL DEFAULT '2',
  PRIMARY KEY (`Sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1001', 't123456', '张老师', ' 男 ', '32', '', '1');
INSERT INTO `t_user` VALUES ('1002', 't1002', '赵老师', ' 男 ', '21', '', '1');
INSERT INTO `t_user` VALUES ('1006', 't1005', '许老师', ' 男 ', '29', '', '1');
INSERT INTO `t_user` VALUES ('2018090001', '090001', 'xiaotian1', ' 男 ', '20', '信息安全技术', '2');
INSERT INTO `t_user` VALUES ('2018090002', '1111111', 'xiaotian2', ' 男 ', '20', '电子商务', '2');
INSERT INTO `t_user` VALUES ('2018090003', '090003', 'xiaotian3', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090004', '090004', 'xiaotian4', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090005', '090005', '小天', ' 女 ', '20', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090006', '090006', 'xiaotian5', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090007', '090007', 'xiaotian6', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090010', '090010', 'xiaotian7', ' 男 ', '21', '信息安全技术', '2');
INSERT INTO `t_user` VALUES ('2018090011', '090011', 'xiaotian8', ' 男 ', '21', '信息安全技术', '2');
INSERT INTO `t_user` VALUES ('2018090012', '090012', 'xiaotian9', ' 男 ', '21', '信息安全技术', '2');
INSERT INTO `t_user` VALUES ('2018090013', '090013', 'xiaotian10', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090014', '090014', 'xiaotian11', ' 男 ', '21', '信息安全技术', '2');
INSERT INTO `t_user` VALUES ('2018090015', '090015', 'xiaotian12', ' 男 ', '21', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090016', '090016', 'xiaotian13', ' 男 ', '21', '商务英语', '2');
INSERT INTO `t_user` VALUES ('2018090017', '090017', 'xiaotian14', ' 男 ', '21', '商务英语', '2');
INSERT INTO `t_user` VALUES ('2018090020', '090020', 'cjy1', ' 女 ', '19', '商务英语', '2');
INSERT INTO `t_user` VALUES ('2018090030', '090030', '小王', ' 男 ', '11', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090033', '090033', '小天', ' 男 ', '25', '软件技术', '2');
INSERT INTO `t_user` VALUES ('2018090035', '090035', 'xz', '男', '20', '软件技术', '2');
INSERT INTO `t_user` VALUES ('admin', 'admin', 'admin', '男', '0', '0', '0');
